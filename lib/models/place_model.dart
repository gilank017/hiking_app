class Places {
  String id;
  String name;
  String description;
  String location;
  String image;

  Places({this.id, this.name, this.description, this.location, this.image});

  factory Places.fromjson(Map<String, dynamic> json) => Places(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      location: json['location'],
      image: json['image']);
}
