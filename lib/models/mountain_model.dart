class DataModel {
  String id;
  String name;
  String location;
  String image;

  DataModel({this.id, this.name, this.location, this.image});

  factory DataModel.fromjson(Map<String, dynamic> json) => DataModel(
      id: json['id'],
      name: json['name'],
      location: json['location'],
      image: json['image']);
}
