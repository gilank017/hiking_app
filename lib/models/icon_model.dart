class DataIcon {
  String id;
  String name;
  String icon;

  DataIcon({this.id, this.name, this.icon});

  factory DataIcon.fromjson(Map<String, dynamic> json) =>
      DataIcon(id: json['id'], name: json['name'], icon: json['icon']);
}
