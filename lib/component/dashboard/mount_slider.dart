import 'package:flutter/material.dart';
import 'package:demo_project/models/mountain_model.dart';

class MountSlider extends StatelessWidget {
  final data = [
    DataModel(
        id: "1",
        name: "Mt Bromo",
        location: "Indonesia",
        image: "images/image1.jpg"),
    DataModel(
        id: "2",
        name: "Mt Fuji",
        location: "Japan",
        image: "images/image2.jpg"),
    DataModel(
        id: "3",
        name: "Mt Penangggungan",
        location: "Indonesia",
        image: "images/image3.jpg"),
    DataModel(
        id: "4",
        name: "Mt Arjuna",
        location: "Indonesia",
        image: "images/image4.jpg"),
    DataModel(
        id: "5",
        name: "Mt Semeru",
        location: "Indonesia",
        image: "images/image5.jpg"),
    DataModel(
        id: "6",
        name: "Mt Jaya Wijaya",
        location: "Indonesia",
        image: "images/image6.jpg"),
    DataModel(
        id: "7",
        name: "Mt Kelud",
        location: "Indonesia",
        image: "images/image7.png"),
    DataModel(
        id: "8",
        name: "Mt Krakatau",
        location: "Indonesia",
        image: "images/image8.jpg"),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              "Most Hiked This Month",
              style: TextStyle(
                fontFamily: 'Avenir',
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 15.0),
            height: MediaQuery.of(context).size.height * 0.21,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: data.length,
              itemBuilder: (context, index) {
                return Container(
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: Card(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(4.0),
                            child: Image.asset(
                              '${data[index].image}',
                              fit: BoxFit.cover,
                              height: 80,
                              width: 100,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          margin: EdgeInsets.fromLTRB(12, 5, 0, 0),
                          child: Text(
                            data[index].name,
                            style: TextStyle(
                                fontSize: 14.0, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.left,
                          ),
                        ),
                        Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.fromLTRB(10, 2, 0, 0),
                            child: Row(
                              children: <Widget>[
                                Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
                                    child: Icon(Icons.location_on, size: 10)),
                                Text(data[index].location,
                                    style: TextStyle(fontSize: 14))
                              ],
                            ))
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
