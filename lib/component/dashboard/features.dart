import 'package:flutter/material.dart';
import 'package:demo_project/models/icon_model.dart';

class Features extends StatelessWidget {
  final icon = [
    DataIcon(
        id: "1", name: "Administration", icon: "images/administration.png"),
    DataIcon(id: "2", name: "Email Me", icon: "images/email.png"),
    DataIcon(id: "3", name: "My Location", icon: "images/navigation.png"),
    DataIcon(id: "4", name: "Search", icon: "images/search.png"),
    DataIcon(id: "5", name: "My Team", icon: "images/teamwork.png"),
    DataIcon(id: "6", name: "Contact Us", icon: "images/technical.png"),
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              "Categories",
              style: TextStyle(
                fontFamily: 'Avenir',
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
            height: MediaQuery.of(context).size.height * 0.15,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: icon.length,
              itemBuilder: (context, index) {
                return Container(
                  width: MediaQuery.of(context).size.width * 0.2,
                  child: Card(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Image.asset(
                            '${icon[index].icon}',
                            fit: BoxFit.cover,
                            height: 30,
                            width: 30,
                          ),
                        ),
                        Container(
                            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: Text(
                              icon[index].name,
                              style: TextStyle(fontSize: 10),
                            )),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
