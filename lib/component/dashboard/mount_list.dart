import 'package:flutter/material.dart';
import 'package:demo_project/models/place_model.dart';

class MountList extends StatelessWidget {
  final description = [
    Places(
      id: "1",
      name: "Mt Semeru",
      location: "Indonesia",
      description:
          "Tanjung bira terletak di daerah Sulawesi Selatan. Lokasi khususnya adalah Kabupaten Bulukumba yang jaraknya sangat jauh, sekitar 200 km dari pusat ibukota Makassar. Sehingga tanjung bira bisa diaktakan terletak di ujung selatan daratan sulawesi selatan. Tanjung bira memang bisa dikatakan senjata oleh masyarakat sekitar untuk menarik masyarakat luar dan wisatawan baik lokal maupun asing untuk datang ke sana. Tanjung bira tentu saja menawarkan hal seperti pantai putih, alam bawah laut dan juga pemandangan senja yang tidak ada duanya.",
      image: "images/image1.jpg",
    ),
    Places(
      id: "2",
      name: "Mt Arjuna",
      location: "Indonesia",
      description:
          "Soal indah, Pantai Bara dan Pantai Bira ibarat saudara kembar. Selain namanya yang mirip, letaknya sama-sama di Bulukumba, Sulsel. Pantai pasir putih nan lembut dan laut biru ada di sini. Pantai Bara memang belum setenar Tanjung Bira. Meski demikian, pemandangan di sini tak kalah menarik. Hamparan pasir putih dipadukan dengan tenangnya laut dari arah Flores, membuat kita betah berlama-lama di Pantai Bara.",
      image: "images/image2.jpg",
    ),
    Places(
      id: "3",
      name: "Mt Bromo",
      location: "Indonesia",
      description:
          "Di sini kamu dapat menemukan banyak rumah tongkonan yang telah berdiri semenjak zaman leluhur. Di atas bagian pintu masuk kamu dapat menemukan kepala kerbau yang ditempelkan di dinding. Dalam budaya masyarakat Toraja, hal tersebut memiliki arti kemakmuran, kejayaan, dan status sosial.",
      image: "images/image3.jpg",
    ),
    Places(
      id: "4",
      name: "Mt Mahameru",
      location: "Indonesia",
      description:
          "Suku Kajang Ammatoa terletak di kabupaten Bulukumba, Kecamatan Kajang, Sulawesi Selatan. Desa ini dinamakan Tana Toa yang merupakan tanah yang tertua di dunia dikarenakan kepercayaan masyarakat adatnya. Secara geografis, luas wilayah Desa Kajang Ammatoa sekitar 331,17 ha dan memiliki kondisi hutan yang sangat lebat. Hampir seluruh dusun yang berada di dalamnya di kelilingi hutan dan tidak ada jalan beraspal di dalam kawasan ini.",
      image: "images/image4.jpg",
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              "Choosen For You",
              style: TextStyle(
                fontFamily: 'Avenir',
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
            child: ListView.builder(
              shrinkWrap: true,
              physics: ScrollPhysics(),
              itemCount: description.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Card(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Container(
                              margin: EdgeInsets.all(10),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(4.0),
                                child: Image.asset(
                                  '${description[index].image}',
                                  fit: BoxFit.cover,
                                  height: 170,
                                  width: 130,
                                ),
                              ),
                            )),
                        Expanded(
                            flex: 3,
                            child: Container(
                              margin: EdgeInsets.fromLTRB(15, 20, 0, 0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    description[index].name,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 24.0,
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.only(top: 10.0),
                                      child: Row(
                                        children: <Widget>[
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 0, 5, 0),
                                              child: Icon(Icons.location_on,
                                                  size: 10)),
                                          Text(description[index].location,
                                              style: TextStyle(fontSize: 14))
                                        ],
                                      )),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10.0),
                                    child: Text(description[index].description,
                                        maxLines: 5,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          color: Colors.black54,
                                        )),
                                  )
                                ],
                              ),
                            )),
                        Container(
                          margin: EdgeInsets.all(15.0),
                          child: Icon(Icons.favorite_border_outlined,
                              size: 20.0, color: Colors.pinkAccent),
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
